#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

import math
from typing import Optional, Tuple

import cv2
import magic
import os

from numpy.core.multiarray import ndarray
from wand.image import Image as WImage

from imagecomparator.model.exceptions import LoadError
from imagecomparator.model.watchable import Watchable


class Image(Watchable):
    class Action(Watchable.Action):
        MARKER_SET = 0

    class Quality:
        def __init__(self, ext: str, lossless: bool, value: int):
            self.ext = ext
            self.value = value
            self.lossless = lossless

        def __gt__(self, other):
            if not isinstance(other, Image.Quality):
                raise TypeError(f"Cannot compare {Image.Quality.__name__} with {type(other)}")
            return self.lossless and not other.lossless or self.ext == other.ext and self.value > other.value

        def __lt__(self, other):
            if not isinstance(other, Image.Quality):
                raise TypeError(f"Cannot compare {Image.Quality.__name__} with {type(other)}")
            return not self.lossless and other.lossless or self.ext == other.ext and self.value < other.value

        def __eq__(self, other):
            if not isinstance(other, Image.Quality):
                raise TypeError(f"Cannot compare {Image.Quality.__name__} with {type(other)}")
            return self.lossless and other.lossless or self.ext == other.ext and self.value == other.value

        @classmethod
        def from_wimage(cls, wimage: WImage) -> Image.Quality:
            format_map = {
                'PNG': ('png', True, 100),
                'JPEG': ('jpeg', False, wimage.compression_quality),
                'WEBP': ('webp', wimage.compression_quality == 100, 0)
            }
            return cls(*format_map.get(wimage.format, (None, None, None)))

    size_units = ['B', 'KB', 'MB', 'GB']

    def __init__(self, path: str):
        super().__init__()
        self.path: str = os.path.realpath(path)
        self._data: Optional[ndarray] = None
        # True when we have reading error or file was removed
        self.skip: bool = False
        self.size: Optional[Tuple[int, int]] = None
        self._quality: Optional[Image.Quality] = None
        self.file_size: int = os.path.getsize(path)
        self._marker_id: Optional[int] = None
        # Index should be set and changed by ResultGroup
        # Have in mind this is index in a ResultGroup and it differs form index used by Sections
        self.index: Optional[int] = None

    def __repr__(self):
        return f'Image({os.path.basename(self.path)})'

    @property
    def ratio(self):
        # Width / Height
        return self.size[0] / self.size[1]

    @property
    def filename(self):
        return os.path.basename(self.path)

    @property
    def file_size_str(self) -> str:
        b = self.file_size
        for num, unit in reversed(list(enumerate(self.size_units))):
            size = math.pow(1024, num)
            if b > size:
                return f'{round(b/size)} {unit}'

    @property
    def quality(self) -> Image.Quality:
        if self._quality is None:
            wimg = WImage(filename=self.path)
            self._quality = Image.Quality.from_wimage(wimg)
        return self._quality

    @property
    def ssim_data(self) -> ndarray:
        if self._data is None:
            arr_color = cv2.imread(self.path)
            if arr_color is None:
                self.skip = True
                raise LoadError(self, self.path)
            self.size = arr_color.shape[1], arr_color.shape[0]
            arr_color = cv2.resize(arr_color, (100, 100))
            self._data = cv2.cvtColor(arr_color, cv2.COLOR_BGR2GRAY)
        return self._data

    @property
    def marker_id(self) -> int:
        return self._marker_id

    @marker_id.setter
    def marker_id(self, value: int):
        self._marker_id = value
        self.run_handlers(self.Action.MARKER_SET)

    @staticmethod
    def is_image(path: str):
        if not os.path.exists(path):
            print(f"'{path}' does not exist.")
            return False
        mime = magic.from_file(path, mime=True)
        return mime.startswith('image/')

    def is_identical(self, other: Image) -> bool:
        return (self._data == other._data).all()

    def delete(self):
        self.skip = True  # TODO; thread safety
        self._data = None
        os.remove(self.path)
