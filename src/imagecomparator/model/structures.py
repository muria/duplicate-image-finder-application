#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from collections import Hashable
from typing import Dict, Optional, Any


class TwoWayKey:
    def __init__(self, first, second):
        if not isinstance(first, Hashable) or not isinstance(second, Hashable):
            raise TypeError('Given arguments must be hashable.')
        if hash(first) < hash(second):
            self.first = first
            self.second = second
        else:
            self.first = second
            self.second = first

    def __eq__(self, p):
        if not isinstance(p, self.__class__):
            return False
        return self.first == p.first and self.second == p.second or self.first == p.second and self.second == p.first

    def __hash__(self):
        return hash((self.first, self.second))


class TwoWayDict:
    def __init__(self):
        self._map: Dict[TwoWayKey, Any] = {}
        self._singles: Dict[Any, Dict] = {}

    def all_by_key_part(self, key_part):
        return self._singles.get(key_part, {})

    def __setitem__(self, instance, value):
        if not isinstance(instance, TwoWayKey):
            raise KeyError(f"Key must be of type {TwoWayKey.__name__}")
        self._map[instance] = value
        singles = self._singles.get(instance.first, None)
        if singles is None:
            singles = {}
            self._singles[instance.first] = singles
        singles[instance.second] = value

        singles = self._singles.get(instance.second, None)
        if singles is None:
            singles = {}
            self._singles[instance.second] = singles
        singles[instance.first] = value

    def __getitem__(self, item):
        return self._map[item]

    def get(self, *args):
        self._map.get(*args)


def cmp_all(a, b) -> Optional[int]:
    """
    Returns
        0 : when elements are equal
        1 : when all elements of a >= b
        -1: when all elements of b >= a
        None: otherwise
    """
    if len(a) != len(b):
        return None
    eq = all(i == j for i, j in zip(a, b))
    if eq:
        return 0
    agt = all(i >= j for i, j in zip(a, b))
    if agt:
        return 1
    bgt = all(i <= j for i, j in zip(a, b))
    if bgt:
        return -1
    return None
