#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import pathlib

from kivy.uix.image import Image

assets_dir = pathlib.Path(__file__).parent


def asset(filename: str) -> str:
    return str(assets_dir.joinpath(filename))


class Layout:
    MAIN = asset('layout.kv')


class Settings:
    VIEW = asset('settingsview.json')
    SHORTCUTS = asset('shortcuts.json')


class Colors:
    BACKGROUND = (0, 0, 0, 1)
    FIELD = (.1, .1, .1, 1)
    SELECTION = (.5, .5, 1, .3)
    NONE = (1, 1, 1, 0)


class Textures:
    DEL_MARK = Image(source=asset('mark.png')).texture
    COG = Image(source=asset("cog.png")).texture
    INFO = Image(source=asset("info.png")).texture
