#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from collections import Callable
from enum import Enum
from typing import Dict, List


class Watchable:
    class Action(Enum):
        pass

    def __init__(self):
        self.handlers: Dict[Watchable.Action, List[Callable]] = {}

    def run_handlers(self, action: Action, *args):
        for handler in self.handlers.get(action, []):
            handler(*args)

    def on(self, action: Action, func: Callable):
        if not isinstance(action, self.Action):
            raise TypeError(f'Improper action for {self.__class__.__name__}')
        handlers = self.handlers.get(action, None)
        if handlers is None:
            handlers = []
            self.handlers[action] = handlers
        handlers.append(func)

    def off(self, action: Action, func: Callable):
        handlers = self.handlers.get(action, [])
        if func in handlers:
            handlers.remove(func)

    def remove_handlers(self, action: Action = None):
        if action is None:
            self.handlers.clear()
        else:
            self.handlers.pop(action)
