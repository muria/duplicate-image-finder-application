#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

import os
from enum import Enum
from typing import Union, Callable, List, Set, Tuple, Dict

from imagecomparator.model.image import Image
from imagecomparator.model.structures import cmp_all
from imagecomparator.model.watchable import Watchable


class ResultGroup(Watchable):
    class Action(Watchable.Action):
        IMAGE_ADDED = 0
        IMAGE_REMOVED = 1
        MARKER_FOUND = 2

    def __init__(self):
        super().__init__()
        self.images: [Image] = []
        self.marker_groups: List[List[Image]] = []
        self.unmarked_images: Set[Image] = set()

    def shared_path(self) -> str:
        if not self.images:
            return ''
        shared = os.path.split(self.images[0].path)[:-1]
        for path in (os.path.split(image.path)[:-1] for image in self.images[1:]):
            i = 0
            while i < len(shared) and i < len(path) and shared[i] == path[i]:
                i += 1
            shared = shared[:i]
        return '/'.join(shared) + '/'

    def add_image(self, image: Image):
        if image not in self.images:
            image.index = len(self.images)
            self.images.append(image)
            self._add_to_marker_group(image)
            self.run_handlers(ResultGroup.Action.IMAGE_ADDED, image)

    def _add_to_marker_group(self, image: Image):
        for marker_id, marker_group in enumerate(self.marker_groups):
            if image.is_identical(marker_group[0]):
                image.marker_id = marker_id
                marker_group.append(image)
                break

        if image.marker_id is None:
            found = None
            for other in self.unmarked_images:
                if image.is_identical(other):
                    marker_id = len(self.marker_groups)
                    image.marker_id = marker_id
                    other.marker_id = marker_id
                    self.marker_groups.append([other, image])
                    found = other
                    break
            if found:
                self.unmarked_images.remove(found)
                self.run_handlers(ResultGroup.Action.MARKER_FOUND, found)
            else:
                self.unmarked_images.add(image)

    def absorb(self, group: ResultGroup):
        for image in group:
            self.add_image(image)

    def remove(self, rem: Union[Image, List[Image]]):
        if isinstance(rem, Image):
            rem = [rem]
        for item in rem:
            if item in self.images:
                self.images.remove(item)
        for i, image in enumerate(self.images):
            image.index = i
        self.run_handlers(self.Action.IMAGE_REMOVED, self, rem)

    class TopAttribute(Enum):
        BEST_FILE_SIZE = 1
        WORST_FILE_SIZE = 2
        BEST_RESOLUTION = 3
        WORST_RESOLUTION = 4
        BEST_QUALITY = 5
        WORST_QUALITY = 6

    @staticmethod
    def get_top_attribute(images: List[Image], attribute: ResultGroup.TopAttribute):
        attribute_map: Dict[ResultGroup.TopAttribute, Tuple[Callable, Callable]] = {
            ResultGroup.TopAttribute.BEST_FILE_SIZE: (lambda img: img.file_size, lambda a, b: 0 if a == b else 1 if a < b else -1),
            ResultGroup.TopAttribute.WORST_FILE_SIZE: (lambda img: img.file_size, lambda a, b: 0 if a == b else 1 if a > b else -1),
            ResultGroup.TopAttribute.BEST_RESOLUTION: (lambda img: img.size, lambda a, b: cmp_all(a, b)),
            ResultGroup.TopAttribute.WORST_RESOLUTION: (lambda img: img.size, lambda a, b: cmp_all(b, a)),
            ResultGroup.TopAttribute.BEST_QUALITY: (lambda img: img.quality, lambda a, b: 1 if a > b else 0 if a == b else -1 if a < b else None),
            ResultGroup.TopAttribute.WORST_QUALITY: (lambda img: img.quality, lambda a, b: 1 if a < b else 0 if a == b else -1 if a > b else None),
        }
        key, cmp = attribute_map[attribute]

        result = []
        ambiguous = set()
        imgs = images[1:]
        result.append(images[0])
        for image in imgs:
            if ambiguous:
                # If we have incomparable they can be replaced by now one if cpm returns 1 for all
                include = True
                for current in ambiguous.copy():
                    cmp_res = cmp(key(image), key(current))
                    if cmp_res == 1:
                        ambiguous.discard(current)
                    elif cmp_res == -1:
                        include = False
                        break
                if not ambiguous:
                    result.append(image)
                elif include:
                    ambiguous.add(image)
            else:
                current = result[0]
                res = cmp(key(image), key(current))
                if res is None:
                    # Comparison not possible
                    ambiguous = set(result)
                    result.clear()
                elif res == 0:
                    result.append(image)
                elif res > 0:
                    result.clear()
                    result.append(image)
        # clear if all elements are equal
        if result == images:
            result = []
        return result

    def __iter__(self):
        class Iterator:
            def __init__(self, group):
                self.group = group
                self.next = 0

            def __next__(self):
                if len(self.group.images) > self.next:
                    self.next += 1
                    return self.group.images[self.next - 1]
                raise StopIteration

        return Iterator(self)

    def __getitem__(self, index):
        return self.images[index]

    def __len__(self):
        return len(self.images)
