#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

import math
from enum import Enum
from typing import Optional, List, Tuple, Callable, Set

from kivy import Config
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle
from kivy.metrics import dp
from kivy.uix.image import Image
from kivy.uix.label import Label

from imagecomparator.model.resultgroup import Image as ImageModel
from imagecomparator.model.resultgroup import ResultGroup
from imagecomparator.model.utils import binary_index_search
from imagecomparator.view.utils import trim, between


class CompareMethod(Enum):
    SIDE_BY_SIDE = 0
    ONE_AT_TIME = 1
    SLIDER = 2
    HEATMAP = 3

    def min_select(self) -> int:
        min_map = {
            CompareMethod.SIDE_BY_SIDE: 2,
            CompareMethod.ONE_AT_TIME: 1,
            CompareMethod.HEATMAP: 2,
            CompareMethod.SLIDER: 2
        }
        return min_map[self]

    def max_select(self) -> int:
        max_map = {
            CompareMethod.SIDE_BY_SIDE: None,
            CompareMethod.ONE_AT_TIME: 1,
            CompareMethod.HEATMAP: 2,
            CompareMethod.SLIDER: 2
        }
        return max_map[self]


class ScalingMethod(Enum):
    SIZE = 0
    ZOOM = 1


class InfoLabel(Label):
    pass


class ZoomLabel(Label):
    pass


class TopAttributes:
    class Attribute:
        def __init__(self):
            self.best = []
            self.worst = []

    def __init__(self):
        self.file_size = TopAttributes.Attribute()
        self.resolution = TopAttributes.Attribute()
        self.quality = TopAttributes.Attribute()


class Sections:
    """Helps with drawing on canvas."""
    ZOOM_MIN: float = .1
    ZOOM_MAX: float = 10
    ZOOM_STEP: float = .1
    SEPARATOR_SIZE = 1

    def __init__(self, widget: 'CompareWidget'):
        self.widget: 'CompareWidget' = widget
        self.sections: List[Section] = []
        # Section used for calculating scale in
        self.leading_section: Optional[Section] = None
        self.width: int = 0
        self.section_size: Optional[Tuple[float, float]] = None
        self.shift: Tuple[float, float] = .5, .5
        self.top_attributes = TopAttributes()

    @property
    def canvas(self):
        return self.widget.view.image_view.canvas

    @property
    def view_pos(self) -> Tuple[float, float]:
        return self.widget.view.image_view.pos

    def is_scaling_mode(self) -> bool:
        return self.widget and self.widget.view.scale_size.state == 'down'

    def is_info_visible(self) -> bool:
        return self.widget and self.widget.view.info_button.state == 'down'

    def calculate_sections(self):
        self.clear()
        selected_indices: List[int] = self.widget.selected
        self._update_sizes()
        for index, global_index in enumerate(selected_indices):
            image: ImageModel = self.widget.group.images[global_index]
            self.sections.append(Section(self, index, image))

        self.zoom_fit()
        self.update_comparisons()
        self.draw()

    def update_sections(self):
        """Updates sections after adding/removing images"""
        selected_images: List[ImageModel] = self.widget.selected_images
        old_images: List[ImageModel] = [s.image for s in self.sections]

        self._update_sizes()

        # Remove unselected sections
        del_indexes = []
        for i, section in enumerate(self.sections):
            if section.image not in selected_images:
                del_indexes.append(i)
            else:
                section.update_pos()
                section.update_fit_zoom()
                section.__shift_range = None
        for i in reversed(del_indexes):
            del self.sections[i]
        for image in selected_images:
            if image not in old_images:
                index = binary_index_search(image, self.sections, lambda img, s: img.index > s.image.index)
                section = Section(self, index, image)
                self.sections.insert(index, section)

        self.leading_section = None
        self.scale_update()
        # scale_update won't correct leading_section, so we do that manually
        self.leading_section.zoom = self.leading_section.zoom * (self.leading_section.fit_zoom / self.leading_section.fit_zoom)

        self.update_comparisons()
        self.draw()

    def calculate_leading_section(self):
        """Sets section with smallest default zoom as leading section"""
        if self.leading_section is None and len(self.sections):
            self.leading_section = self.sections[0]
            for section in self.sections[1:]:
                if section.fit_zoom < self.leading_section.fit_zoom:
                    self.leading_section = section

    def _update_sizes(self):
        if len(self.widget.selected):
            available = self.widget.view.image_view.size[0] - self.SEPARATOR_SIZE * (len(self.widget.selected) - 1)
            self.width = int(available / len(self.widget.selected))
            self.section_size = self.width, self.widget.view.image_view.size[1]

    def adjust_zoom_proportions(self):
        self.calculate_leading_section()
        for section in self.sections:
            if section is not self.leading_section:
                section.zoom = self.leading_section.zoom * (section.fit_zoom / self.leading_section.fit_zoom)

    def clear(self):
        self.sections.clear()
        self.leading_section = None
        self.width = 0
        self.shift = .5, .5

    def draw(self, shift: Tuple[float, float] = (0, 0)):
        self.canvas.clear()
        for section in self.sections:
            section.draw(shift)

    def scale_update(self):
        """Updates scales after switching scaling mode"""
        if self.is_scaling_mode():
            self.adjust_zoom_proportions()
        else:
            for section in self.sections:
                section.zoom = self.leading_section.zoom
        self.draw()

    def zoom_toggle(self):
        if self.leading_section:
            if self.leading_section.zoom == self.leading_section.fit_zoom:
                self.zoom_100()
            else:
                self.zoom_fit()
        self.draw()

    def zoom_fit(self):
        if not self.sections:
            return
        self.calculate_leading_section()
        if self.is_scaling_mode():
            self.leading_section.zoom = self.leading_section.fit_zoom
            self.adjust_zoom_proportions()
        else:
            for section in self.sections:
                section.zoom = self.leading_section.fit_zoom
        self.draw()

    def zoom_100(self):
        if not self.sections:
            return
        if not self.is_scaling_mode():
            for section in self.sections:
                section.zoom = 1
        else:
            self.calculate_leading_section()
            self.leading_section.zoom = 1
            self.adjust_zoom_proportions()
        self.shift = (.5, .5)
        self.draw()

    def zoom_in(self):
        self._zoom_step(self.ZOOM_STEP)

    def zoom_out(self):
        self._zoom_step(-self.ZOOM_STEP)

    def _zoom_step(self, step):
        """
        Changes zoom by step. Zooms in in step is positive. Zooms out if step is negative.
        Zoom stops if either min, max is reached.
        Zoom anchors on 100% or when image fills canvas.
        """
        if not self.sections:
            return
        stops = []
        scale = False
        # If we scale images, zoom must scale as well
        if self.is_scaling_mode():
            scale = True
            self.calculate_leading_section()

        for section in self.sections:
            s = section.fit_zoom / self.leading_section.fit_zoom if scale else 1
            end = section.zoom + step * s
            if between(section.fit_zoom, section.zoom, end):
                stops.append((section.fit_zoom - section.zoom) * s)
            if between(1, section.zoom, end):
                stops.append((1 - section.zoom) * s)
            if end > self.ZOOM_MAX:
                stops.append((self.ZOOM_MAX - section.zoom) * s)
            if end < self.ZOOM_MIN:
                stops.append((self.ZOOM_MIN - section.zoom) * s)

        if stops:
            min_stop: float = stops[0]
            for stop in stops:
                if math.fabs(stop) < math.fabs(min_stop):
                    min_stop = stop
            step = min_stop

        if scale:
            self.leading_section.zoom += step
            self.adjust_zoom_proportions()
        else:
            for section in self.sections:
                section.zoom += step

    def move(self, shift: Tuple[float, float]):
        if not self.sections:
            return
        section = self.leading_section if self.leading_section else self.sections[0]
        self.shift = section.get_shift_after_change(shift)

    def update_comparisons(self):  # TODO; Refactor usage
        if self.widget.group:
            images = [i.image for i in self.sections]
            self.top_attributes.file_size.best = self.widget.group.get_top_attribute(images,ResultGroup.TopAttribute.BEST_FILE_SIZE)
            self.top_attributes.file_size.worst = self.widget.group.get_top_attribute(images, ResultGroup.TopAttribute.WORST_FILE_SIZE)
            self.top_attributes.resolution.best = self.widget.group.get_top_attribute(images, ResultGroup.TopAttribute.BEST_RESOLUTION)
            self.top_attributes.resolution.worst = self.widget.group.get_top_attribute(images, ResultGroup.TopAttribute.WORST_RESOLUTION)
            self.top_attributes.quality.best = self.widget.group.get_top_attribute(images, ResultGroup.TopAttribute.BEST_QUALITY)
            self.top_attributes.quality.worst = self.widget.group.get_top_attribute(images, ResultGroup.TopAttribute.WORST_QUALITY)


class Section:
    def __init__(self, parent: Sections, index: int, image: ImageModel):
        self.parent: Sections = parent
        self.index: int = index
        self._zoom: Optional[float] = None
        # Holds zoom value for which image fits section
        self._fit_zoom: Optional[float] = None
        self.__shift_range: Optional[Tuple[float, float]] = None
        self.view_pos: Optional[Tuple[float, float]] = None
        self.update_pos()
        self._load_image(image)

    @property
    def size(self) -> Tuple[float, float]:
        return self.parent.section_size

    def _load_image(self, image):
        self.image: ImageModel = image
        # TODO use existing image data if possible
        self.texture = Image(source=self.image.path).texture
        self._fit_zoom = None

    @property
    def canvas(self):
        return self.parent.canvas

    @property
    def ratio(self):
        return self.size[0] / self.size[1]

    @property
    def zoom(self) -> float:
        return self._zoom

    @zoom.setter
    def zoom(self, value: float):
        self._zoom = value
        self._shift_range = None

    def update_pos(self):
        self.view_pos = (self.parent.view_pos[0] + self.index * (self.parent.width + Sections.SEPARATOR_SIZE),
                         self.parent.view_pos[1])

    def change_current(self, image):
        old_fill_zoom = self.fit_zoom
        self._load_image(image)
        if old_fill_zoom != self.fit_zoom:
            if self.parent.is_scaling_mode():
                self.zoom = self.zoom * (self.fit_zoom / old_fill_zoom)
            else:
                # TODO;
                pass

    @property
    def fit_zoom(self) -> float:
        if self._fit_zoom is None:
            if self.image.ratio > self.ratio:
                zoom = self.size[0] / self.image.size[0]
            else:
                zoom = self.size[1] / self.image.size[1]
            self._fit_zoom = trim(zoom, Sections.ZOOM_MIN, Sections.ZOOM_MAX)
        return self._fit_zoom

    def update_fit_zoom(self):
        self._fit_zoom = None

    @property
    def _shift_range(self):
        """Property holding range, to not calculate it each time when moving image."""
        if self.__shift_range is None:
            self.__shift_range = ((self.image.size[0] - self.size[0] / self.zoom),
                                 (self.image.size[1] - self.size[1] / self.zoom))
        return self.__shift_range

    @_shift_range.setter
    def _shift_range(self, value: Optional[Tuple[float, float]]):
        self.__shift_range = value

    def get_shift_after_change(self, change: Tuple[float, float]) -> Tuple[float, float]:
        # Remember that change is in pixels but shift in %
        shift_x = None
        shift_y = None
        if self.image.size[0] * self.zoom < self.size[0]:
            shift_x = .5
        if self.image.size[1] * self.zoom < self.size[1]:
            shift_y = .5
        if shift_x is None:
            s_x = change[0] / self._shift_range[0] / self.zoom if self._shift_range[0] else 0
            shift_x = trim(self.parent.shift[0] + s_x, 0, 1)
        if shift_y is None:
            s_y = change[1] / self._shift_range[1] / self.zoom if self._shift_range[1] else 0
            shift_y = trim(self.parent.shift[1] + s_y, 0, 1)
        return shift_x, shift_y

    def real_pos(self, x, y):
        return self.view_pos[0] + x, self.view_pos[1] + y

    def draw(self, shift: Tuple[float, float] = (0, 0)):
        with self.canvas:
            # TODO; Try mapping results once calculated and see if it speeds up application
            # self._shift_range = (self.image.size[a] - self.size[a] / self.zoom)
            shift_x, shift_y = self.get_shift_after_change(shift)
            tx = max(.0, self._shift_range[0] * shift_x)
            ty = max(.0, self._shift_range[1] * shift_y)
            tw = min(self.size[0] / self.zoom, self.image.size[0])
            th = min(self.size[1] / self.zoom, self.image.size[1])

            texture = self.texture.get_region(tx, ty, tw, th)

            x = max(.0, (self.size[0] - self.image.size[0] * self.zoom) / 2)
            y = max(.0, (self.size[1] - self.image.size[1] * self.zoom) / 2)
            w = min(self.size[0], self.image.size[0] * self.zoom)
            h = min(self.size[1], self.image.size[1] * self.zoom)

            Color(1, 1, 1)
            Rectangle(texture=texture, pos=self.real_pos(x, y), size=(w, h))

        config = Config.get_configparser('app')

        if self.parent.is_info_visible():
            info_label = InfoLabel()
            shadow = InfoLabel()
            info_label.text, shadow.text = self.info_text()
            info_label.halign = shadow.halign = 'left'
            info_label.valign = shadow.valign = 'top'
            info_label.font_size = shadow.font_size = config.getint('info', 'font_size')

            def set_size(label, size):
                position = config.get('info', 'position')
                if position == 'Top':
                    label.pos = self.real_pos(dp(6), self.size[1] - size[1] - dp(6))
                else:
                    label.pos = self.real_pos(dp(6), 0)
                shadow.pos = (label.pos[0] + dp(1), label.pos[1] - dp(1))
                info_label.size = shadow.size = (self.parent.width - dp(12), label.texture_size[1])

            info_label.bind(texture_size=set_size)
            shadow.color = (0, 0, 0, 1)
            self.parent.widget.view.image_view.add_widget(shadow)
            self.parent.widget.view.image_view.add_widget(info_label)

        if config.getboolean('info', 'zoom_show'):
            font_size = config.getint('info', 'zoom_font_size')
            position = config.get('info', 'zoom_position')
            zoom_label = ZoomLabel(font_size=font_size, text=f'[b]{round(self.zoom * 100, 2)} %[/b]')

            def set_size(label, size):
                label.size = size[0] + dp(14), size[1] + dp(4)
                if position == 'Top':
                    label.pos = self.real_pos(self.size[0] - label.size[0] - dp(10), self.size[1] - label.size[1] - dp(10))
                else:
                    label.pos = self.real_pos(self.size[0] - label.size[0] - dp(10), dp(10))

            zoom_label.bind(texture_size=set_size)
            self.parent.widget.view.image_view.add_widget(zoom_label)

    @staticmethod
    def quality_str(image: Image) -> str:
        q = image.quality
        if q is not None and q.ext is not None:
            value = "lossless" if q.lossless else q.value if q.value else "lossy"
            return f'{value} ({q.ext})'
        return 'Unknown'

    @staticmethod
    def color_text(text: str, color: str):
        return f'[color={color}]{text}[/color]'

    def info_text(self) -> Tuple[str, str]:
        config = Config.get_configparser('app')
        info = ''
        shadow = ''
        best_color = config.get('info', 'attribute_color_best')
        worst_color = config.get('info', 'attribute_color_worst')
        if config.getboolean('info', 'path'):
            path_type = config.get('info', 'path_type')
            if path_type == "Full":
                info += self.image.path
            elif path_type == "Cut shared":
                info += self.image.path[len(self.parent.widget.group.shared_path()):]
            elif path_type == "Name only":
                info += self.image.filename
            shadow = info

        attributes: List[Tuple[str, Callable, TopAttributes.Attribute]] = [
            ('size', lambda i: f'{i.size[0]}x{i.size[1]}', self.parent.top_attributes.resolution),
            ('file_size', lambda i: self.image.file_size_str, self.parent.top_attributes.file_size),
            ('quality', lambda i: self.quality_str(i), self.parent.top_attributes.quality)]

        for attribute_name, string, top in attributes:
            if config.getboolean('info', attribute_name):
                if info:
                    info += '\n'
                    shadow += '\n'
                    line = string(self.image)
                    shadow += line
                    if self.image in top.best:
                        line = Section.color_text(line, best_color)
                    elif self.image in top.worst:
                        line = Section.color_text(line, worst_color)
                    info += line

        return info, shadow
