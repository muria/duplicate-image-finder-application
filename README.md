# Duplicate Image Finding Application

This project is a prototype of [picture-comparator](https://gitlab.com/muria/picture-comparator).

It uses very simple (and extremely slow) comparing algorithm, which was later changed.  
It's also based on Kivi, to see how this framework works, but it was later replaced with PySide6 (Qt for Python).
