#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from kivy.properties import BooleanProperty, NumericProperty
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.widget import Widget


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior, RecycleBoxLayout):
    pass


class SelectableRecycleView(RecycleView):
    selected_index = NumericProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SelectableWidget(RecycleDataViewBehavior, Widget):
    index = NumericProperty(None, allownone=True)
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def refresh_view_attrs(self, rv, index, data):
        self.index = index
        return super(SelectableWidget, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch):
        if super(SelectableWidget, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv: SelectableRecycleView, index: int, is_selected: bool):
        self.selected = is_selected

        if is_selected:
            rv.selected_index = index
        elif rv.selected_index == index:
            rv.selected_index = None

    def on_press(self):
        for child in self.parent.children:
            child.selected = False
        self.selected = True
